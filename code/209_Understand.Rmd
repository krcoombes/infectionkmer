---
title: "Displaying the Relative Risk of Infection"
author: "Cat Coombes and Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    theme: yeti
    highlight: kate
    toc: yes
  pdf_document:
    toc: yes
---

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE)
# knitr::opts_chunk$set(fig.path = "./figs/")
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Introduction
Paths:
```{r paths}
source("00-paths.R")
```

Next, we load in our log odd computations.
```{r kmers}
load(file.path(paths$data, "AlphabetSoupStrings3.Rda"))
load(file.path(paths$scratch, "Odds.Rda"))
```

```{r merset}
merpath <- file.path(paths$scratch, "Mers", "TF")
merset <- new.env()
load(file.path(merpath, "OneMersTF.Rda"), merset)
load(file.path(merpath, "DiMersTF.Rda"), merset)
load(file.path(merpath, "TriMersTF.Rda"), merset)
load(file.path(merpath, "FourMersTF.Rda"), merset)

```

```{r computeRR}
computeRR <- function(U, measure = LRR, menv = merset) {
  N <- length(U)
  if (N < 8) {
    stop("Event string is too short.\n")
  }
  trim <- function(X) {
    X[X < -0.5] <- -0.49999
    X
  }
  
  attach(menv)
  on.exit(detach(menv))
  ## singletons
  lax <- trim(matrix(measure[U]))

  ## dimers
  X <- U[1 : (2*trunc(N/2))]
  M <- apply(matrix(X, nrow=2), 2, paste, collapse="")
  temp <- measure[M]
  temp[is.na(temp)] <- 0
  lam <- as.matrix(rep(temp, each=2))
  lam <- rbind(lam, matrix(rep(NA, N - length(lam))))

  X <- U[-1]
  W <- X[1 : (2*trunc(length(X)/2))]
  M <- apply(matrix(W, nrow = 2), 2, paste, collapse="")
  temp <- measure[M]
  temp[is.na(temp)]  <- 0
  lam2 <- as.matrix(rep(temp, each=2))
  lam2 <- rbind(NA, lam2)
  lam2 <- rbind(lam2, matrix(rep(NA, N - length(lam2))))

  ## trimers
  X <- U[1 : (3*trunc(N/3))]
  M <- apply(matrix(X, nrow = 3), 2, paste,collapse="")
  temp <- measure[M]
  temp[is.na(temp)] <- 0
  lay <- as.matrix(rep(temp, each = 3))
  lay <- rbind(lay, matrix(rep(NA, N - length(lay))))

  X <- U[-1]
  W <- X[1 : (3*trunc(length(X)/3))]
  M <- apply(matrix(W, nrow = 3), 2, paste, collapse="")
  temp <- measure[M]
  temp[is.na(temp)] <- 0
  lay2 <- as.matrix(rep(temp, each = 3))
  lay2 <- rbind(NA, lay2)
  lay2 <- rbind(lay2, matrix(rep(NA, N - length(lay2))))

  X <- U[-(1:2)]
  W <- X[1 : (3*trunc(length(X)/3))]
  M <- apply(matrix(W, nrow = 3), 2, paste, collapse="")
  temp <- measure[M]
  temp[is.na(temp)] <- 0
  lay3 <- as.matrix(rep(temp, each = 3))
  lay3 <- rbind(matrix(rep(NA, 2)), lay3)
  lay3 <- rbind(lay3, matrix(rep(NA, N - length(lay3))))

  ## fourmers
  X <- U[1 : (4*trunc(N/4))]
  M <- apply(matrix(X, nrow = 4), 2, paste,collapse="")
  temp <- measure[M]
  temp[is.na(temp)] <- 0
  laz <- as.matrix(rep(temp, each = 4))
  laz <- rbind(laz, matrix(rep(NA, N - length(laz))))

  X <- U[-1]
  W <- X[1 : (4*trunc(length(X)/4))]
  M <- apply(matrix(W, nrow = 4), 2, paste, collapse="")
  temp <- measure[M]
  temp[is.na(temp)] <- 0
  laz2 <- as.matrix(rep(temp, each = 4))
  laz2 <- rbind(NA, laz2)
  laz2 <- rbind(laz2, matrix(rep(NA, N - length(laz2))))

  X <- U[-(1:2)]
  W <- X[1 : (4*trunc(length(X)/4))]
  M <- apply(matrix(W, nrow = 4), 2, paste, collapse="")
  temp <- measure[M]
  temp[is.na(temp)] <- 0
  laz3 <- as.matrix(rep(temp, each = 4))
  laz3 <- rbind(matrix(rep(NA, 2)), laz3)
  laz3 <- rbind(laz3, matrix(rep(NA, N - length(laz3))))

  X <- U[-(1:3)]
  W <- X[1 : (4*trunc(length(X)/4))]
  M <- apply(matrix(W, nrow = 4), 2, paste, collapse="")
  temp <- measure[M]
  temp[is.na(temp)] <- 0
  laz4 <- as.matrix(rep(temp, each = 4))
  laz4 <- rbind(matrix(rep(NA, 3)), laz4)
  laz4 <- rbind(laz4, matrix(rep(NA, N - length(laz4))))

  V <- cbind(lax, lam, lam2,
             lay, lay2, lay3,
             laz, laz2, laz3, laz4)
  list(U = U, V = V)
}

```

```{r showRR}
clap <- colorRampPalette(c("darkblue", "gray", "darkred"))(96)
clap <- clap[c(seq(1, 47, 2), 49:96)]
showRR <- function(relrisk, vl = NULL, 
                   showColorBar = FALSE, setLayout = FALSE,
                   col = clap, main = "", cex= NA) {
  N <- nrow(relrisk$V)
  if (is.na(cex)) {
    cex <- ifelse(N < 50, 1,
           ifelse(N < 100, 0.8,
           ifelse(N < 150, 0.75,
                  0.5)))
  }

  if (showColorBar) {
    if (setLayout) {
      opar <- par("mfrow")
      on.exit(par(opar))
      layout(matrix(1:2, nrow=1), heights = 1, widths = c(1,7))
    }
    # left panel, colorbar
    par(mai = c(1.4, 1.2, 1.4, 0.2))
    ranger <- seq(-0.5, 1, length = 64)
    image(1, ranger, matrix(ranger, nrow = 1), xaxt = "n", xlab = "",
          col = clap, ylab = "Log Relative Risk")
  }
  ## right panel, main image
  par(mai = c(1.22, 1.22, 0.82, 0.42))
  rv <- relrisk$V
  rv[rv > 1] <- 1
  image(1:N, 1:10, rv, xlab="String Position",
        yaxt = "n", ylab = "K", main = main,
        col = clap, zlim=c(-0.5, 1))
  if (any(vl > 0)) abline(v = vl - 0.5)
  mtext(c(1, 2, 2, 3, 3, 3, 4, 4, 4, 4),
        side = 2, line = 1, at = 1:10)
  mtext(relrisk$U, side = 3, line = 1/4, at = 1:N, cex = cex)
  abline(h = c(1.5, 3.5, 6.5))
}
```

```{r testAgain, fig.cap="Relative risk image."}
S <- soup$my.string[52]
U <- strsplit(S, "")[[1]]
rr <- computeRR(U)
showRR(rr)
```

```{r fig.cap="Relative risk image with color bar.", fig.width=14, fig.height = 6}
if(FALSE) windows(width=14, height=6)
showRR(rr, showColorBar = TRUE, setLayout = TRUE)
```

```{r saver}
save(computeRR, showRR, clap, file = file.path(paths$scratch, "showRR.rda"))
```

# Appendix
This analysis was performed in this environment:
```{r si}
sessionInfo()
```
and in the following directory:
```{r where}
getwd()
```
