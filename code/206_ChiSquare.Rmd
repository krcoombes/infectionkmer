---
title: "Chi-Squared Tests"
author: "Cat Coombes and Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    theme: yeti
    highlight: kate
    toc: yes
  pdf_document:
    toc: yes
---

```{r setup, include=FALSE, results="hide"}
options("encoding" = "UTF-8")
knitr::opts_chunk$set(echo = TRUE)
# knitr::opts_chunk$set(fig.path = "./figs/")
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Introduction

We load ths summary counts for k-mers (with $2\le k \le 12$).
```{r data}
source("00-paths.R")
load(file.path(paths$scratch, "AllCounts.Rda"))
```

Here is a summary of the number of k-mers for different k. Note that these were chosen
using a criterion that insisted they had to be seen in at least 100 patients (roughly
2% of the total).
```{r merlength}
merlength <- nchar(colnames(AllCounts))
table(merlength)
```

Now load the IDs and outcome categories.
```{r ids}
load(file.path(paths$data, "AlphabetSoupStrings3.Rda"))
dim(AllCounts)
dim(soup)
summary(soup)
```

Our goal is to perform chi-squared tests comparing groups 1 and 2 (defined by whether or
not the patient6 ever received antibiotics). As usual, we cache the results in a file so
we don't have to wait to recompute it while cleaning up the presentation of the results. 

```{r chisq}
chiResults <- matrix(NA, ncol = 8, nrow = ncol(AllCounts))
AC <- 1 * (AllCounts > 0)
OUT <- soup$group
f <- file.path(paths$scratch, "chiResults.Rda")
if (file.exists(f)) {
  load(f)
} else {
  for (I in 1: ncol(AllCounts)) {
    X <- table(Group = OUT, MER = AC[, I])
    Y <- 100*X[,2]/apply(X, 1, sum)
    chir <- chisq.test(X[1:2,]) # just use "double" pos or neg groups
    chiResults[I,] <- c(chir$statistic, chir$p.value,
                        as.vector(X), # four columns
                        Y # four columns
                        )
  }
  colnames(chiResults) <- c("chi", "p.value", 
                            paste("G", 1:2, ".Absent", sep = ""),
                            paste("G", 1:2, ".Present", sep = ""),
                            paste("G", 1:2, ".Percent", sep = ""))
  Delta <- chiResults[, "G2.Percent"] - chiResults[, "G1.Percent"]
  chiResults <- data.frame(KMER = colnames(AllCounts), LEN = merlength, 
                      chiResults, Delta = Delta)
  save(chiResults, file = f)
  library(readr) # to preseve Greek letters in CSV file
  write_excel_csv(chiResults, file = file.path(paths$results,
                                               "chiResults.csv")) # so Excel knows it is UTF-8
  rm(I, X, Y, chir)
}
rm(f)
```

Here is an overview, focusing on the p-values.
```{r fig.cap = "Distribution of chi-squared p-values."}
summary(chiResults)
hist(chiResults$p.value, breaks = 123)
```

We see that there are spikes both near zero (interesting) and near one (much smaller,
but exceptionally
boring). We use our `fitmix3` function from the `NewmanOmics` pacakge to decompose
the p-value distribution.
```{r newm, fig.cap = "Fitting models to p-values."}
library(NewmanOmics)
fit <- fitMix3(chiResults$p.value) # a pile of wearnings
sum(chiResults$p.value < 0.01)
plot(fit) # poor fit
hist(fit) # clearly wring
image(fit) # evrything is near zero
```

Here are the top 200 k-mers by p-value significance
```{r top200}
#summary(chiResults[chiResults$p.value < 0.01,]) # 6.637
load(file.path(paths$delirium, "greek.Rda"))
X <- colnames(AC)[order(chiResults$p.value)][1:200]
for (I in 1:20) {
  cat(cement(X[(1 + 10*(I-1)):(10*I)]), "\n")
}
```

```{r echo=FALSE, eval =FALSE}
tab <- table(Group = OUT$ICD, MER = AC[, "PVPVPVPVPV"])
tab
tab[,2]/apply(tab,1, sum)

tab <- table(Group = OUT$ICD, MER = AC[, "PIXKΛ"])
tab
tab[,2]/apply(tab,1, sum)

```



```{r}
library(beanplot)
beanplot(chiResults$chi ~ merlength)
abline(h = 6.637, col = "red")
```

# Appendix
This analysis was performed in this environment:
```{r si}
sessionInfo()
```
and in the following directory:
```{r where}
getwd()
```