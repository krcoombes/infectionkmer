---
title: "ROC Curve"
author: "Cat Coombes, Naleef Fareed, and Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    theme: yeti
    highlight: kate
    toc: yes
  pdf_document:
    toc: yes
---

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE)
# knitr::opts_chunk$set(fig.path = "./figs/")
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Introduction
Paths:
```{r paths}
source("00-paths.R")
```

Next, we load in our log odd computations.
```{r kmers}
load(file.path(paths$scratch, "fullAnalysis.rda"))
load(file.path(paths$data, "AlphabetSoupStrings3.Rda"))
```



```{r}
MAGIC <- exp(median(LRR))
f <- file.path(paths$scratch, "maxgain.Rda")
if (file.exists(f)) {
  load(f)
} else {
  Delta <- sapply(soup$my.string, function(S) {
    U <- strsplit(S, "")[[1]]
    rsk <- try(computeRisk(U))
    if (inherits(rsk, "try-error")) {
      return(NA)
    }
    pdis <- computePDisease(rsk)
    N <- length(pdis[[12]])
    delta <- pdis[[12]] - MAGIC*timeprob[1:N] 
    delta
  })
  maxgain <- sapply(Delta, max)
  save(Delta, maxgain, file = f)
}
rm(f)
```

```{r fig.cap="Distributions."}
class(maxgain)
length(maxgain)
summary(maxgain)
hist(maxgain, breaks=234)
library(beanplot)
beanplot(maxgain ~ soup$group, what = c(1, 1, 1, 0))
qqplot(maxgain[soup$group==1], maxgain[soup$group == 2])
abline(0, 1)
```

```{r fig4, fig.cap = "ROC curve.", fig.width=7, fig.height=7}
library(pROC)
soup$maxgain <- maxgain
stew <- soup[!is.na(soup$maxgain), ]
dim(stew)

myROC <- roc(group ~ maxgain, data = stew, ci = TRUE)
plot(myROC, print.auc = TRUE)
resn <- 300
dev.copy(png, file = file.path(paths$results, "Figure4.png"),
         width = 6*resn, height=5*resn, res = resn)
dev.off()
```


```{r fig.cap = "ROC curve.", echo=FALSE, eval=FALSE}
library(ROCR)
soup$maxgain <- maxgain
stew <- soup[!is.na(soup$maxgain), ]
dim(stew)
fooroc <- roc(stew$group, stew$maxgain, ci = TRUE, of = "coords")
plot(fooroc, ci=TRUE, ci.type = "shape")
pred <- prediction(stew$maxgain, stew$group)
perf <- performance(pred, "tpr", "fpr")
plot(perf)
abline(0,1)
```

```{r cutoff}
W <- which.min(abs(myROC$sensitivities - 0.8))
myROC$specificities[W]
target <- myROC$predictor[W]
target

continge <- table(HIGH = stew$maxgain > target, INFECTION = stew$group)
continge

diag(continge)/apply(continge, 2, sum)
```


This analysis was performed in this environment:
```{r si}
sessionInfo()
```
and in the following directory:
```{r where}
getwd()
```
