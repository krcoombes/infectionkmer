```
author: Cat Coombes and Kevin R. Coombes
date: 2022-08-29
title: Results Area
```

# Overview
This folder is designed to hold the results of the analyses. Tables
are usually stored as (UTF-8 encoded) CSv files, and figures are
usually stored as PNG files.
