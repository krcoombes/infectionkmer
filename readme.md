```
author: Cat Coombes and Kevin R. Coombes
date: 2022-08-29
title: Modeling EHR Infection Using Proteomics-Like K-mers
```

# Overview
This git project provides supporting material for the manuscript
"_Sequences of Events from the Electronic Medical Record and the
Onset of Infection_" by Caitlin E. Coombes, Kevin R. Coombes, and
Naleef Fareed, It contains complete data and code to reproduce the
analyses described in that paper, including all figures and tables.

# Instructions
The organizational structure used here is the one described in the
git project at: https://gitlab.com/krcoombes/organizer Specifically,
the idea is to keep the code in git, but to simplify the linkage
between code, data, scratch computations, and results. 

## Defining Paths to Folders
Every Rmarkdown script in the code repository starts by `source`ing
the file `00-paths.R`. That script, in turn, looks for a specific file
in JavaScript Object Notation (JSON) called `antibiotics3.json` inside
a `Paths` dierectory inside the users `$HOME` directory. (On a Windows
machine, the `$HOME` directory is assumed, by default, to be
`C:\Users\[yourname]`.) An example version of `antibiotics3.json` is
included at the top level of the `code` directory.

You can look at that file to see which locations need to be defined
on your machine. If you are willing to put everything into the same
space occupied by a clone of this git project, then you can simply
create a `Paths` folder in your home directory and copy the example
file into that folder. Alternatively, you can edit the locations to
put things anywhere you want. Only the `data` folder is included in
the git repository. If you move that elsewhere, you need to copy or
move the contents of the folder to wherever you want to store the
data.

## Running the Code
After setting up your json file to describe your folder locatiosn, you
should run each of the Rmarkdown files in numerical order. Some of
these tun quite quickly; other are quite time-consuming. For all of
the long sompurations, teh scripts explictly compute the results and
cache tem in the `scratch` space that you have defined. That way,
everything runs quickly the second time around. All of the tables and
5 of the 6 figures are automatically stored in the `results`
folder. The excpetion is Figure 6, which is an overview of the main
algoritm in pseudocode. To produce that figure, you should run the
script `doc/Version1/methods.R` from the `doc` folder.

  Cat and Kevin
